/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui.info.report;

import org.json.JSONArray;
import java.io.FileOutputStream;
import java.io.OutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.json.JSONObject;
import util.file.DataFile;

/**
 *
 * @author matheus
 */
public class Report {

    private JSONArray json;
    private JSONObject jOb;
    private String fileName;
    private PdfPTable table = new PdfPTable(4);
    private PdfPCell[] cell = new PdfPCell[4];

    public Report(JSONArray json, String fileName) {
        this.json = json;
        this.fileName = fileName;
    }

    public void exportPDF() throws IOException {
        Document doc = null;
        OutputStream outStream = null;
        PdfPRow row = new PdfPRow(buildTableHeader("Data","Descrição","Valor","Status"));
        table.getRows().add(row);
        try {
            doc = new Document(PageSize.A4, 30, 72, 72, 30);
            outStream = new FileOutputStream(fileName);
            PdfWriter.getInstance(doc, outStream);
            doc.open();
            doc.add(new Paragraph(30, buildHeader()));
            for (int i = 0; i < json.length(); i++) {
                jOb = new JSONObject(json.get(i).toString());
                row = new PdfPRow(buildTableHeader(jOb.getString("data"), jOb.getString("descricao"),"R$ " + String.valueOf(jOb.getDouble("valor")) + '0', jOb.getString("estado")));
                table.getRows().add(row);
            }
            doc.add(table);

        } catch (FileNotFoundException | DocumentException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (doc != null) {
                doc.close();
            }
            if (outStream != null) {
                outStream.close();
            }
        }
        JOptionPane.showMessageDialog(null, "PDF Salvo com sucesso!");
    }

    private String buildHeader() throws IOException {
        DataFile file = new DataFile("dados.ttu");
        JSONObject json = new JSONObject(file.load());
        return repeat(" ", 45) + "Tesoureiro responsavel: " + json.getString("name") +"\n\n";
    }

    private PdfPCell[] buildTableHeader(String cell1,String cell2,String cell3,String cell4) {
        PdfPCell[] cell = new PdfPCell[]{
            new PdfPCell(Phrase.getInstance(cell1)),
            new PdfPCell(Phrase.getInstance(cell2)),
            new PdfPCell(Phrase.getInstance(cell3)),
            new PdfPCell(Phrase.getInstance(cell4))
        };
        return cell;
    }
    private static final String repeat(String str, int times) {
    if (str == null) return null;
    
    StringBuilder sb = new StringBuilder();
    for (int i = 0 ; i < times ; i ++) {
      sb.append(str);
    }
    return sb.toString();
  }
}
