/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author matheus
 */
public class Transacao {

    private String descricao;
    private double valor;
    private String estado;
    private String data;

    public Transacao(String descricao, double valor, String estado) {
        this.descricao = descricao;
        this.valor = valor;
        this.estado = estado;
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy 'às' hh:mm:ss a");
        data = ft.format(dNow);
    }

    public Transacao(String str) {
        JSONObject ob = new JSONObject(str);
        this.descricao = ob.getString("descricao");
        this.estado = ob.getString("estado");
        this.valor = ob.getDouble("valor");
        this.data = ob.getString("data");
    }

    @Override
    public String toString() {
        return "{" + "\"descricao\": \"" + descricao + "\",\"valor\": " + valor + ", \"estado\": \""
                + estado + "\",\"data\": \"" + data + "\"}";
    }

}
