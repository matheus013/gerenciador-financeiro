/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetos;

import org.json.JSONObject;

/**
 *
 * @author matheus
 */
public class Tesoureiro {

    private String name;
    private String setor;
    private double pago;
    private double pagar;
    private double recebido;
    private double receber;

    public Tesoureiro(String name, String setor, double pago, double pagar, double recebido, double receber) {
        this.name = name;
        this.setor = setor;
        this.pago = pago;
        this.pagar = pagar;
        this.recebido = recebido;
        this.receber = receber;
    }

    @Override
    public String toString() {
        return "{\"name\": \"" + name + "\", \"setor\": \"" + setor + "\", \"pago\": " + pago + ", \"pagar\": " + pagar + ", \"recebido\": " + recebido + ", \"receber\": " + receber + '}';
    }

}
