/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author matheus
 */
public class DataFile {

    private final String fileName;
    private final String langstr = "[{\"descricao\": \"Stater\",\"valor\": 0.0,\"estado\": \"Exemplo, favor apagar para evitar problemas futuros\",\"data\": \"00.00.0000 às 00:00:00 AM\"}]";

    public DataFile(String fileName) {
        this.fileName = fileName;
    }
    public void save(String str) throws IOException {
        File f = new File(fileName);
        if (!f.exists()) {
        } else {
            f.createNewFile();
            try (BufferedWriter file = new BufferedWriter(new FileWriter(f.getCanonicalFile()))) {
                file.write(langstr);
                file.close();
            }
        }
        try (BufferedWriter file = new BufferedWriter(new FileWriter(f.getCanonicalFile()))) {
            file.write(str);
            file.close();
        }

    }

    public String load() throws IOException {
        String str;
        File f = new File(fileName);
        if (!f.exists()) {
            f.createNewFile();
           try (BufferedWriter file = new BufferedWriter(new FileWriter(f.getCanonicalFile()))) {
                file.write(langstr);
                file.close();
            }
        }
        try (BufferedReader file = new BufferedReader(new FileReader(f.getCanonicalFile()))) {
            str = file.readLine();
            file.close();
        }
        return str;
    }
    public boolean isEmpty(){
        File f = new File(fileName);
        return f.length() > 0;
    }

}
