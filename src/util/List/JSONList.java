/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.List;

import java.util.LinkedList;
import java.util.List;
import objetos.Transacao;
import org.json.JSONArray;

/**
 *
 * @author matheus
 */
public class JSONList {

    private JSONArray json;
    private List<String> list = new LinkedList<>();

    public JSONList(String str) {
        if (!str.equals("")) {
            json = new JSONArray(str);
            for (int i = 0; i < json.length(); i++) {
                Transacao t = new Transacao(json.get(i).toString());
                list.add(t.toString());
            }
        } else {
            json = new JSONArray();
        }
    }

    public void add(Transacao transacao) {
        list.add(transacao.toString());
    }

    public String getString() {
        return list.toString();
    }

    public JSONArray getArray() {
        return json;
    }

    public void remove(int id) {
        list.remove(id);
    }

    public void upgrade(int id, Transacao t) {
        list.set(id, t.toString());
    }
}
